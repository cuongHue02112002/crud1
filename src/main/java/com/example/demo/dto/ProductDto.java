package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table
public class ProductDto {
    @Id
    private int id;
    private String name;
    private  String description;
    private  String quantity;
    private String price;
    private String itemId;

}
