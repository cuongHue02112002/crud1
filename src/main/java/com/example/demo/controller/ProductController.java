package com.example.demo.controller;

import com.example.demo.dto.ProductDto;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;
import com.example.demo.repository.ProductReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductReponsitory productReponsitory;
    @PostMapping("/products")
    public ResponseEntity<Product> save(@RequestBody ProductDto productDto){
        return new ResponseEntity<>(productReponsitory.save(
                productMapper.toDoModel(productDto)), HttpStatus.CREATED);
    }
    @GetMapping("/products")
    public ResponseEntity<List<ProductDto>> findAll(){
        return new ResponseEntity<>(productMapper.modelstodos(productReponsitory.findAll()),HttpStatus.OK);
    }
    @GetMapping("/products/{id}")
    public ResponseEntity<ProductDto> findById(@PathVariable(value = "id") Integer id ){
        return new ResponseEntity<>(productMapper.modelToDo(productReponsitory.findById(id).get()),HttpStatus.OK);
    }
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable(value = "id") Integer id ){
        ProductDto productDto = productMapper.modelToDo(productReponsitory.findById(id).get());
        productReponsitory.deleteById(productDto.getId());
        return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
